<?php

namespace Beigenut\RequestFlow\Controller\Page;
use \Magento\Framework\App\RequestInterface as Request;

class CustomNoRouteHandler implements \Magento\Framework\App\Router\NoRouteHandlerInterface
{
    public function process(Request $request)
    {
        $request->setRouteName('noroutefound')->setControllerName('page')->setActionName('customnoroute');
        return true;
    }
}

