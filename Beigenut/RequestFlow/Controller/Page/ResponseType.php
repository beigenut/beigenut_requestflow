<?php


namespace Beigenut\RequestFlow\Controller\Page;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Controller\Result\Raw;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\Controller\Result\RedirectFactory;

class ResponseType extends Action
{

    protected $pageFactory;

    protected $jsonFactory;

    protected $raw;

    protected $forwardFactory;

    protected $redirectFactory;

    //    PageFactory 는 magento 기본 theme front page 를 보여준다
    public function __construct(
      Context $context,
      PageFactory $pageFactory,
      JsonFactory $jsonFactory,
      Raw $raw,
      ForwardFactory $forwardFactory,
      RedirectFactory $redirectFactory
    ) {
        $this->pageFactory     = $pageFactory;
        $this->jsonFactory     = $jsonFactory;
        $this->raw             = $raw;
        $this->forwardFactory  = $forwardFactory;
        $this->redirectFactory = $redirectFactory;

        parent::__construct($context);
    }

    public function execute()
    {
        //        url : http://107.21.119.77/noroutefound/page/responsetype

        //        마젠토 기본 디폴트 페이지 띰을 생성한다
        //        return $this->pageFactory->create();

        //        JSON 포맷의 데이터 값을 불러와 저장한다
        //        return $this->jsonFactory->create()->setData([
        //          'key'  => 'value',
        //          'key2' => ['one', 'two'],
        //        ]);

        //        raw 형태의 데이터를 불러온다 ex) echo 'hello world' 와 동일
        //        $result = $this->raw->setContents('hello world');
        //        return $result;

        //        Forward 타입 데이터 결과 : 브라우저 url 변경없이 보이는 페이지만 가져옴
        //        $result = $this->forwardFactory->create();
        //        $result->setModule('noroutefound')->setController('page')->forward('customnoroute');
        //        return $result;

        //        Redirect 타입 결과 : 브라우저 url 이 변경되면서 아예 페이지 이동
        $result = $this->redirectFactory->create();
        $result->setPath('noroutefound/page/customnoroute');
        return $result;
    }

}