<?php


namespace Beigenut\RequestFlow\Controller\Page;

use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ActionFactory;

class Router implements \Magento\Framework\App\RouterInterface
{

    protected $actionFactory;

    public function __construct(ActionFactory $actionFactory)
    {
        $this->actionFactory = $actionFactory;
    }

    public function match(RequestInterface $request)
    {

        //  /customer-account-login 이라고 타이핑 하는 것을 아래와 같이 변경해주는
        //  /customer/account/login
        $path  = trim($request->getPathInfo(), '/');
        $paths = explode('-', $path);
        $request->setModuleName($paths[0]);
        $request->setControllerName($paths[1]);
        $request->setActionName($paths[2]);

        return $this->actionFactory->create('Magento\Framework\App\Action\Forward',
          ['request' => $request]);
    }

}