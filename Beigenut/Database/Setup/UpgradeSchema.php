<?php


namespace Beigenut\Database\Setup;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $setup->startSetup();

        if(version_compare($context->getVersion(), '1.0.1', '<')){
            $setup->getConnection()->addColumn(
              $setup->getTable('affiliate_member'),
              'phone_number',
              ['nullable'=>false, 'type'=>Table::TYPE_TEXT, 'comment'=>'PHONE NUMBER OF MEMBER']
            );
        }

        $setup->endSetup();
    }
}