<?php


namespace Beigenut\Database\Model;
use Beigenut\Database\Api\AffiliateMemberRepositoryInterface;
use Beigenut\Database\Model\ResourceModel\AffiliateMember\CollectionFactory;

class AffiliateMemberRepository implements AffiliateMemberRepositoryInterface
{

    private $collectionFactory;

    public function __construct(
      CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return \Beigenut\Database\Api\Data\AffiliateMemberInterface[]|void
     */
    public function getList()
    {
       return $this->collectionFactory->create()->getItems();
    }

}