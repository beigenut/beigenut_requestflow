<?php
namespace Beigenut\Database\Model\ResourceModel\AffiliateMember;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Beigenut\Database\Model\AffiliateMember;
use Beigenut\Database\Model\ResourceModel\AffiliateMember as AffiliateMemberResource;

class Collection extends AbstractCollection
{
    protected function _construct() {
        parent::_construct();
        $this->_init(AffiliateMember::class, AffiliateMemberResource::class);
    }

}